# NextPCB JP Kicad用プラグイン

### あなたの基板レイアウトを1クリックで24時間以内に送信します。
プラグインを導入し、高品質なプリント基板と実装サービスを利用しましょう。NextPCBはお客様の素晴らしい製品作りに貢献する準備ができています。

NextPCB JPプラグインボタンをクリックすると、プロジェクトからこれらのファイルをエクスポートします。
1. ガーバファイル
2. IPCネットリストファイル
3. すべての部品データを含むBomファイル
4. 実装サービス用pick & placeファイル

"Save to Cart"をクリックすることでファイルをアップロード(数秒かかります)後、即座に注文が可能です。注文後私たちのエンジニアによってファイルの検査が２度行われます。

### KiCAD公式レポジトリからのインストール
KiCADのメインメニューより"Plugin and Content Manager" を開き、リストから"NextPCB JP Plug-in for KiCad" を選択してインストールします。

### 手動インストール
以下のリンクから最新のzipファイルをインストールしてください。
（https://gitlab.com/nextpcbofficial/NextPCB-JP-Plug-in-for-Kicad） KiCADのメインウィンドウから"Plugin and Content Manager" に入り、"Install from File"から先ほどダウンロードしたzipファイルを開いてください。.

### BOMとは
設計に使用されているすべての部品のデータにアクセスできます。
以下の情報を用いて部品のデータ引用の時間を短縮できます。
1. デジグネータ（必須）
2. 数量（必須）
3. 部品番号（必須）
4. パッケージ、フットプリント（必須）
5. 製造会社（選択）
6. 説明、価格（選択　）


### NextPCBとは
NextPCBは中国に本社と工場を置く、基板の試作と組み立てを専門とするメーカーです。
基板の試作とバッジ生産に関するワンストップサービスや実装サービス(SMT)、部品調達、
品質テスト、グローバル配達、そして24時間以内の迅速な対応をしております。

Kicadのスポンサーの一社として、私たちは無料で基板を提供し、あなたが電気の世界をお楽しみいただける事を願っています。

### License and attribution

This plugin is published under the [MIT License](./LICENSE) and is based on [AislerHQ/PushForKiCad](https://github.com/AislerHQ/PushForKiCad).
